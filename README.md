REACT CONTACT MANAGER

Introduction

This is a basic contact list application which is based on React JS Library. In this application, i am fetching data through API and showing it on frontend.

How to install

# Install frontend dependencies
npm install

How to run

After installing dependencies you need to write the command.

npm run start

This will run on the server at localhost:3000. If all is working well, you should be able to access the url http://localhost:3000 from your Browser.

