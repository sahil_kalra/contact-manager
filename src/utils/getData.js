
import axios from "axios";

const URL = "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json";
export const getData = async () => {
	return await axios.get(URL).then(res => res.data).catch(err => console.log(err.message) ) ;	
};

export const getSorted = (contacts) =>{
	return contacts.sort((a,b) => a.last_name < b.last_name ? -1 : 1 ); 
}

	