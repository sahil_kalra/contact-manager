import React,{Component} from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Avatars from './Avatar';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';


class List extends Component
{
	constructor(props)
	{
        super(props);
        this.state = {active:0,toggle:false}
	    this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e,id)
	{
		if(this.state.active === id)
		{
			this.setState({
				toggle : !this.state.toggle
			})
		}
		else
		{
			this.setState({
				active : id
			})
		}
		console.log(id);
	}
    	
    render()
    {        
        const active = this.state.active;
		const contacts = [...this.props.contacts];
        return (
	        <Card raised="true" className="card"> 
            <div>
                {contacts.map((contact) => ( 
                    <div className="card" key = {contact.id} onClick = {(e) => this.handleChange(e,contact.id) } >
                    <div className="card-body">
                    <CardContent>
                        <Avatars url = {contact.avatar} />
                        <span className="contact-detail"> 
                            <Typography variant="h5" className="card-title">{contact.first_name} {contact.last_name}</Typography>
                            <Typography variant="h6" className="card-subtitle mb-2 text-muted">{contact.email}</Typography> 
                        </span>
                    <Checkbox className="custom-checkbox" size = "small" checked = {active === contact.id ? ( this.state.toggle === true ? false : true) : false } style = {{color:"#fff"}} />
                    </CardContent>
                    </div>
                    </div>
                ))}
            </div>
        </Card>
	  );	
	}
	
}

export default List;