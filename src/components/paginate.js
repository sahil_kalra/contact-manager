import React,{useState,useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      marginTop: theme.spacing(2),
    },
  },
}));

export default function BasicPagination(props) {
  const classes = useStyles();
  return (
    <div className={classes.root} text-center>
      <div className="center-pagination">
        <Pagination count={50} color="primary" page = {props.page} onChange = {props.handler} />
      </div>
    </div>
  );
}