import React,{useState} from "react";
import TextField from '@material-ui/core/TextField';

const Search = (props) =>{
	const [data,setData] = useState("");
	const styles = {
		width:'100%',
	}
	
	return (
	  	<TextField id="standard-search" label="Search field" type="search" onChange = {props.handlerFilter} value = {props.data} style = {styles} />
	)
	
}

export default Search;