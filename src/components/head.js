import React from "react";

const Head = () =>{ const styles = {
		backgroundColor:"#1ca18d",
		textAlign:"center",
		fontSize:"16px",
		color:"white",
		padding:"10px 20px",
		fontWeight:'500',
	}
	return(
		<h3 style = {styles} >Contacts</h3>
	)
}

export default Head;