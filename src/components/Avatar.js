import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import { teal } from '@material-ui/core/colors';
//import contact from './Contacts';

const useStyles = makeStyles((theme) => ({
  teal: {
    color: theme.palette.getContrastText(teal[300]),
    backgroundColor: teal[50],
  },
}));

export default function FallbackAvatars(props) {
  const classes = useStyles();
  const {url} = props;
  return (
      <Avatar src = {url} className={classes.teal} />
  );
}