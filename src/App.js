import React,{Component} from 'react';
import logo from './logo.svg';
import './App.css';
import {getData,getSorted} from "./utils/getData";
import Head from "./components/head";
import List from "./components/list";
import './App.css';
import { Container } from '@material-ui/core';
import Paginate from "./components/paginate";
import Search from "./components/search";


class App extends Component {
  
  constructor(props)
  {
	super(props);
	this.state = {contacts : [],pagindata :[],page : 1,data:"",filteredData : [] }
	this.handler = this.handler.bind(this);
    this.handlerFilter = this.handlerFilter.bind(this);
  }
  
  componentDidMount()
  {
	getData().then(data => {
		this.setState({
			...this.state,
			contacts : data,
			pagindata:data.slice(0,20),
		});
	}) 
  }
	
  handler(e,val)
	{
		this.setState({
			...this.state,
			pagindata : this.state.contacts.slice( ((val - 1) * 20),(val * 20)),
			page : val
		})
	}

	handlerFilter(e)
	{
		this.setState({
		...this.state,
		data : e.target.value,
		filteredData : this.state.contacts.filter(item => item.first_name.toLowerCase().search(e.target.value.toLowerCase() ) != -1 ).slice(0,20)
		})
	}
  	render()
  	{
	  const paginData = getSorted(this.state.pagindata);
	  const filteredData = getSorted(this.state.filteredData);
      
     return (
  <>
	<div maxWidth = "lg">
		<Head />
		<Container>
			<Search handlerFilter = {this.handlerFilter} data = {this.state.data} />
			<List contacts = { this.state.data.length ? filteredData : paginData } />
			<Paginate handler = {this.handler} page = {this.state.page}/>
		</Container>
	</div>
	</>
  );	  
	 
  }
  
  
}

export default App;
